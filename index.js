const express = require("express");
const bignum = require("bignum");
const Joi = require("joi");
const validation = require("express-joi-validation");

const app = express();
app.use(express.json());

const validator = validation.createValidator();

const schema = Joi.object({
  number: Joi.number().required(),
});
app.get("/cal/:number", validator.params(schema), (req, res) => {
  const { number } = req.params;

  let result = bignum(1);
  const calNumber = bignum(number)
  result = result.add(calNumber)
  result = result.mul(calNumber)
  result = result.div(2)

  console.log("result", typeof result, result.toString());

  res.send({
    result: result.toString(),
  });
});

app.listen(3000, () => {
  console.log("running on port 3000");
});
